#include "Game.h"
#include "Time.h"

#include <iostream>

int main(int argc, char* args[]) {
  Game* game = new Game();
  if (game->Initialise("SDL Game", 640, 640, 0) == -1)
    return -1;

  while (game->IsRunning()) {
    game->HandleEvents();
    game->Update();
    game->Render();

    Time::CalculateDeltaTime();
  }

  delete(game);
  std::cout << "Game closed\n";

  return 0;
}