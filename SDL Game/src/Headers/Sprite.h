#pragma once

class SDL_Rect;
class SDL_Texture;
class Point;

class Sprite {
public:
  Sprite(const char* imagePath);
  Sprite(const char* imagePath, Point position, Point size);
  ~Sprite();

  SDL_Rect* GetSourceRect() { return sourceRect; }
  SDL_Texture* GetTexture() { return texture; }

private:
  void LoadTexture(const char* imagePath);
  SDL_Texture* texture;
  SDL_Rect* sourceRect;
};