#pragma once

#include <SDL.h>

class Time {
public:
	static double GetDeltaTime() { return deltaTime; }
	static void CalculateDeltaTime();

private:
	static double deltaTime;

	static Uint64 ticksThisFrame;
	static Uint64 ticksLastFrame;
};
