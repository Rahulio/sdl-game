#pragma once

class Vector2;

class Transform {
public:
  Transform(Vector2 position, Vector2 scale);

  Vector2* position;
  Vector2* scale;
};