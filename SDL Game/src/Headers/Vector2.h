#pragma once

#include <ostream>

class Point;

class Vector2 {
public:
	Vector2();
	Vector2(float x, float y);
	Vector2(float a);

	Vector2 operator*(float n);
	Vector2 operator/(float n);
	void operator*=(float n);
	void operator/=(float n);

	Vector2 operator*(Vector2 v);
	Vector2 operator/(Vector2 v);
	void operator*=(Vector2 v);
	void operator/=(Vector2 v);

	Vector2 operator+(Vector2 v);
	Vector2 operator-(Vector2 v);
	void operator+=(Vector2 v);
	void operator-=(Vector2 v);

	bool operator==(Vector2 v);
	bool operator!=(Vector2 v);

	friend std::ostream& operator<<(std::ostream& out, const Vector2& v);

	Point ToPoint();

	float x;
	float y;
};