#pragma once

class SDL_Rect;
class Sprite;
class Transform;

class SpriteRenderer {
public:
  SpriteRenderer(Sprite* sprite);
  ~SpriteRenderer();
  void Render(Transform* transform);

  Sprite* sprite;

private:
  SDL_Rect* destinationRect;
  void CalulateDestinationRect(Transform* transform);
};