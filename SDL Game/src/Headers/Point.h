#pragma once

#include <ostream>

class Vector2;

class Point {
public:
	Point();
	Point(int x, int y);
	Point(int a);

	Point operator*(int n);
	Point operator/(int n);
	void operator*=(int n);
	void operator/=(int n);

	Point operator*(Point v);
	Point operator/(Point v);
	void operator*=(Point v);
	void operator/=(Point v);

	Point operator+(Point v);
	Point operator-(Point v);
	void operator+=(Point v);
	void operator-=(Point v);

	bool operator==(Point v);
	bool operator!=(Point v);

	friend std::ostream& operator<<(std::ostream& out, const Point& v);

	Vector2 ToVector2();

	int x;
	int y;
};