#pragma once

class SDL_Window;
class SDL_Renderer;

class Game {
public:
  Game();
  ~Game();
  int Initialise(const char* title, unsigned int width, unsigned int height, bool fullscreen);
  void HandleEvents();
  void Update();
  void Render();

  bool IsRunning() { return isRunning; }
  static SDL_Renderer* GetRenderer() { return renderer; }

  static unsigned int WIDTH;
  static unsigned int HEIGHT;

private:
  SDL_Window* window;
  static SDL_Renderer* renderer;

  bool isRunning;
};