#include "Point.h"
#include "Vector2.h"

Point::Point()
{
  x = y = 0;
}

Point::Point(int x, int y)
{
  this->x = x;
  this->y = y;
}

Point::Point(int a)
{
  this->x = this->y = a;
}

Point Point::operator*(int n)
{
  return Point(x * n, y * n);
}

Point Point::operator/(int n)
{
  return Point(x / n, y / n);
}

void Point::operator*=(int n)
{
  x *= n;
  y *= n;
}

void Point::operator/=(int n)
{
  x /= n;
  y /= n;
}

Point Point::operator*(Point v)
{
  return Point(x * v.x, y * v.y);
}

Point Point::operator/(Point v)
{
  return Point(x / v.x, y / v.y);
}

void Point::operator*=(Point v)
{
  x *= v.x;
  y *= v.y;
}

void Point::operator/=(Point v)
{
  x /= v.x;
  y /= v.y;
}

Point Point::operator+(Point v)
{
  return Point(x + v.x, y + v.y);
}

Point Point::operator-(Point v)
{
  return Point(x - v.x, y - v.y);
}

void Point::operator+=(Point v)
{
  x += v.x;
  y += v.y;
}

void Point::operator-=(Point v)
{
  x -= v.x;
  y -= v.y;
}

bool Point::operator==(Point v)
{
  return (x == v.x && y == v.y);
}

bool Point::operator!=(Point v)
{
  return (x != v.x || y != v.y);
}

Vector2 Point::ToVector2()
{
  return Vector2(x, y);
}

std::ostream& operator<<(std::ostream& out, const Point& v)
{
  out << "(" << v.x << ", " << v.y << ")";
  return out;
}
