#include "SpriteRenderer.h"
#include "Sprite.h"
#include "Game.h"
#include "Transform.h"
#include "Vector2.h"

#include <SDL.h>
#include <SDL_image.h>

SpriteRenderer::SpriteRenderer(Sprite* sprite)
{
  this->sprite = sprite;
  destinationRect = new SDL_Rect{ 0, 0, 0, 0 };
}

SpriteRenderer::~SpriteRenderer()
{
  delete sprite;
  delete destinationRect;
}

void SpriteRenderer::Render(Transform* transform)
{ 
  CalulateDestinationRect(transform);
  SDL_RenderCopy(Game::GetRenderer(), sprite->GetTexture(), sprite->GetSourceRect(), destinationRect);
}

void SpriteRenderer::CalulateDestinationRect(Transform* transform) {
  destinationRect->x = (int)transform->position->x;
  destinationRect->y = (int)transform->position->y;
  destinationRect->w = (int)transform->scale->x;
  destinationRect->h = (int)transform->scale->y;
}
