#include "Vector2.h"
#include "Point.h"

Vector2::Vector2()
{
  x = y = 0;
}

Vector2::Vector2(float x, float y)
{
  this->x = x;
  this->y = y;
}

Vector2::Vector2(float a)
{
  this->x = this->y = a;
}

Vector2 Vector2::operator*(float n)
{
  return Vector2(x * n, y * n);
}

Vector2 Vector2::operator/(float n)
{
  return Vector2(x / n, y / n);
}

void Vector2::operator*=(float n)
{
  x *= n;
  y *= n;
}

void Vector2::operator/=(float n)
{
  x /= n;
  y /= n;
}

Vector2 Vector2::operator*(Vector2 v)
{
  return Vector2(x * v.x, y * v.y);
}

Vector2 Vector2::operator/(Vector2 v)
{
  return Vector2(x / v.x, y / v.y);
}

void Vector2::operator*=(Vector2 v)
{
  x *= v.x;
  y *= v.y;
}

void Vector2::operator/=(Vector2 v)
{
  x /= v.x;
  y /= v.y;
}

Vector2 Vector2::operator+(Vector2 v)
{
  return Vector2(x + v.x, y + v.y);
}

Vector2 Vector2::operator-(Vector2 v)
{
  return Vector2(x - v.x, y - v.y);
}

void Vector2::operator+=(Vector2 v)
{
  x += v.x;
  y += v.y;
}

void Vector2::operator-=(Vector2 v)
{
  x -= v.x;
  y -= v.y;
}

bool Vector2::operator==(Vector2 v)
{
  return (x == v.x && y == v.y);
}

bool Vector2::operator!=(Vector2 v)
{
  return (x != v.x || y != v.y);
}

Point Vector2::ToPoint()
{
  return Point(x, y);
}

std::ostream& operator<<(std::ostream& out, const Vector2& v)
{
  out << "(" << v.x << ", " << v.y << ")";
  return out;
}
