#include "Sprite.h"
#include "Game.h"
#include "Point.h"

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>

Sprite::Sprite(const char* imagePath) {
  LoadTexture(imagePath);
  sourceRect = NULL;
}

Sprite::Sprite(const char* imagePath, Point position, Point size)
{
  LoadTexture(imagePath);
  sourceRect = new SDL_Rect{ position.x, position.y, size.x, size.y };
}

void Sprite::LoadTexture(const char* imagePath) {
  SDL_Surface* surface = IMG_Load(imagePath);
  if (!surface)
    std::cout << "IMAGE LOADING ERROR: " << IMG_GetError() << '\n';

  texture = SDL_CreateTextureFromSurface(Game::GetRenderer(), surface);
  if (!texture) {
    std::cout << "ERROR CREATING TEXTURE: " << imagePath << '\n';
    std::cout << "SDL_ERROR: " << SDL_GetError() << '\n';
  }

  SDL_FreeSurface(surface);
}

Sprite::~Sprite() {
  delete sourceRect;
  SDL_DestroyTexture(texture);
}