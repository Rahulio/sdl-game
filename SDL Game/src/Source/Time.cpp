#include "Time.h"

double Time::deltaTime = 0;
Uint64 Time::ticksThisFrame = 0;
Uint64 Time::ticksLastFrame = 0;

void Time::CalculateDeltaTime()
{
	ticksLastFrame = ticksThisFrame;
	ticksThisFrame = SDL_GetPerformanceCounter();

	deltaTime = (double)((ticksThisFrame - ticksLastFrame) / (double)SDL_GetPerformanceFrequency());
}
