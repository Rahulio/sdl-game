#include "Camera.h"
#include "Game.h"

float Camera::orthographicSize = 0;

Camera::Camera(float orthographicSize) {
  this->orthographicSize = orthographicSize;
}

float Camera::GetUnitSize() {
  return Game::HEIGHT / (orthographicSize * 2.0f);
}